{ lib, rustPlatform, sqlite, openssl, gettext, pkg-config }:

rustPlatform.buildRustPackage rec {
  pname = "hagrid";
  version = "1.0.0";

  src = ./.;
  cargoLock = {
    lockFile = ./Cargo.lock;
    outputHashes = {
      "rocket_i18n-0.5.0" = "sha256-EbUE8Z3TQBnDnptl9qWK6JvsACCgP7EXTxcA7pouYbc=";
    };
  };

  postInstall = ''
    cp -r dist $out
  '';

  nativeBuildInputs = [
    pkg-config
    gettext
  ];

  buildInputs = [
    sqlite
    openssl
  ];

  meta = with lib; {
    description = "A verifying keyserver";
    homepage = "https://gitlab.com/keys.openpgp.org/hagrid";
    license = with licenses; [ gpl3 ];
    maintainers = with maintainers; [ valodim ];
    platforms = platforms.all;
  };
}
